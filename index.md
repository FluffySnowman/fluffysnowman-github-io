---
layout: default
permalink: /
---

<header style="display: contents">
 <font size=1> 
  <div>
    <a style="text-decoration:none" href="{{ "/" | prepend: site.baseurl | replace: '//', '/' }}">
    {% assign owner_first_name = site.owner | split: " " %}
    <h1>fluffy@void:~$</h1>
    </a>
    <div class="header-links">
      {% include links.html %}
    </div>
  </div>
  </font> 
  <hr>
</header>

<br>

[![Hits](https://hits.seeyoufarm.com/api/count/incr/badge.svg?url=https%3A%2F%2Fgithub.com%2FFluffySnowman%2Ffluffysnowman.github.io&count_bg=%2379C83D&title_bg=%23555555&icon=&icon_color=%23E7E7E7&title=hits&edge_flat=false)](https://hits.seeyoufarm.com)

### Welcome to the site where there is content from computer tutorials to photography. Make sure to check out the Indexes on each page for easy navigation :)

## <a href="/jekyll/update/2022/05/26/botnets.html" style="text-decoration:none">Botnet Basics</a>

<br>

## <a href="/jekyll/update/2022/05/18/photography.html" style="text-decoration:none">Photography by The Fluffy Snowman</a>

<br>

## <a href="/jekyll/update/2022/05/06/the-dark-web.html" style="text-decoration:none">The Dark Web</a>

<br>

## <a href="/jekyll/update/2022/04/19/the-hackers-maual.html" style="text-decoration:none">The Hacker's Manual</a>

<br>

## <a href="/jekyll/update/2022/04/17/main.html" style="text-decoration:none">The Guide to Everything</a>

<br>

{% include footer.html %}
